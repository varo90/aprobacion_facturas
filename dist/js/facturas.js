$( document ).ready(function() {
	display_table();
	$('#factura_form').onsubmit = function(event) {
        event.preventDefault();
        $('#send_bill').html('<img src="images/busy.gif"> Subiendo...');
    }
    $('#crear_factura').on('click',function(){
        $('#crear_factura').html('<img src="images/busy.gif" />');
    });
});

function display_table() {
	if($('#data-info').length == false) { return false; }
	var data = {"query" : 'get_facturas_my'};
	var columns = [];
	var rows = [];
	if ( $.fn.DataTable.isDataTable('#data-info') ) {
	  is_dt = true;
	} else {
		var is_dt = false;
	}
	$.ajax({
        data:  data,
        url:   'get-info.php',
        type:  'post',
        dataType:"json",
        beforeSend: function( xhr ) {
            $('#loading').html('<img src="images/busy.gif" />');
        },
        success:  function (response) {
        	$('#loading img').remove();
        	var jsonPretty = JSON.stringify(response.aaData, null, '\t');
        	var jsonObj = $.parseJSON(jsonPretty);
        	$.each(jsonObj[0], function(key,value){
        		columns.push({ mData: key });
        		if(!is_dt) {
            		$('#data-info thead tr').append( $('<th />', {text : key}) );
            		$('#data-info tfoot tr').append( $('<th />', {text : key}) );
        		}
        	});
        	$.each(jsonObj, function(key,value){
        		rows.push(value);
        	});
        	datatable_info(rows,columns);
        }
    });	
}

function datatable_info(response,columns) {
	$('#data-info').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        //"sAjaxSource": "get-info.php",
        "aaSorting": [[0,'desc']],
        "lengthMenu": [[10, 25, 50, 100, 150, 200, 500, 1000, 2000], [10, 25, 50, 100, 150, 200, 500, 1000, 2000]],
        "oLanguage": {
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        "aoColumns": columns,
        "aaData": response
    });
}

function watch_bill(route_file,id,watch_b) {
    if(watch_b == 0) {
        var dialog = bootbox.dialog({
            title: '',
            message: "<iframe width='100%' height='500px' src="+route_file+"></iframe>",
            size: 'large',
            buttons: {
                Denegar: {
                    label: "Denegar",
                    className: 'btn-warning',
                    callback: function(){
                        cambiar_estado(2,id,route_file);

                    }
                },
                Aprobar: {
                    label: "Aprobar",
                    className: 'btn-success',
                    callback: function(){
                        cambiar_estado(1,id,route_file);
                    }
                }
            }
        });
    } else {
        var dialog = bootbox.dialog({
            title: '',
            message: "<iframe width='100%' height='500px' src="+route_file+"></iframe>",
            size: 'large'
        });
    }
}

function cambiar_estado(estado,id,route_file) {
    var params = {"estado":estado,"id":id};
    $.ajax({
        data:  params, //datos que se envian a traves de ajax
        url:   'set_estado_factura.php', //archivo que recibe la peticion
        type:  'post', //método de envio
        // beforeSend: function () {
        //     $("#answers").html("Procesando, espere por favor...");
        // },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            if(estado == 1 && response == 'direcc') {
                upload_to_sharepoint(route_file);
            }
            location.reload();
        }
    });
}

/***********UPLOAD TO SHAREPOINT */
appWebUrl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
hostWebUrl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));

// Upload the file.
// You can upload files up to 2 GB with the REST API.
function upload_to_sharepoint(route_file) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = route_file;

    // Get test values from the file input and text input page controls.
    // The display name must be unique every time you run the example.
    var fileInput = route_file;
    var newName = route_file.split("/").pop();

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    alert('file uploaded and updated');
                });
                changeItem.fail(onError);
            });
            getItem.fail(onError);
        });
        addFile.fail(onError);
    });
    getFile.fail(onError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = jQuery.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(fileInput[0].files[0]);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Get the file name from the file input control on the page.
        var parts = fileInput[0].value.split('\\');
        var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
            "{0}/_api/sp.appcontextsite(@target)/web/getfolderbyserverrelativeurl('{1}')/files" +
            "/add(overwrite=true, url='{2}')?@target='{3}'",
            appWebUrl, serverRelativeUrlToFolder, fileName, hostWebUrl);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Construct the endpoint.
        // The list item URI uses the host web, but the cross-domain call is sent to the
        // add-in web and specifies the host web as the context site.
        fileListItemUri = fileListItemUri.replace(hostWebUrl, '{0}');
        fileListItemUri = fileListItemUri.replace('_api/Web', '_api/sp.appcontextsite(@target)/web');

        var listItemAllFieldsEndpoint = String.format(fileListItemUri + "?@target='{1}'",
            appWebUrl, hostWebUrl);

        // Send the request and return the response.
        return jQuery.ajax({
            url: listItemAllFieldsEndpoint,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {

        // Construct the endpoint.
        // Specify the host web as the context site.
        var listItemUri = itemMetadata.uri.replace('_api/Web', '_api/sp.appcontextsite(@target)/web');
        var listItemEndpoint = String.format(listItemUri + "?@target='{0}'", hostWebUrl);

        // Define the list item changes. Use the FileLeafRef property to change the display name. 
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}",
            itemMetadata.type, newName, newName);

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return jQuery.ajax({
            url: listItemEndpoint,
            type: "POST",
            data: body,
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                "content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Display error messages. 
function onError(error) {
    alert(error.responseText);
}

// Get parameters from the query string.
// For production purposes you may want to use a library to handle the query string.
function getQueryStringParameter(paramToRetrieve) {
    var params = document.URL.split("?")[1].split("&amp;");
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve) return singleParam[1];
    }
}