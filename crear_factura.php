<?php
    include('header.php');
    include('get_data_form.php');
    include('pdo_exceptions.php');
    $success = '';
    if(isset($_GET['success'])) {
        $success = $_GET['success'] == 'yes' ? '<font color="green">La factura se creó correctamente :)</font>' : pdo_exceptions::error_check($_GET['success']);;
    }
?>
<div class="contenedor">
	<div><h3><?php echo $success; ?></h3></div>
	<h1>NUEVA FACTURA</h1>
	<form id="factura_form" class="form-signin" action="set_factura.php" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<input name="nueva_factura" type="file" required="required" class="input_form" />
            <select id="num_prov" name="num_prov" class="selectpicker input_form" data-live-search="true" title="Proveedor..." required="required">
				<?php 
				foreach($proveedores as $proveedor) {
				?>
			 	    <option value="<?php echo $proveedor->id . '__' . $proveedor->name ?>"><?php echo $proveedor->name ?></option>
				<?php 
				}
				?>
            </select>
			<input name="num_fact_prov" type="text" class="form-control input_form" id="num_fact_prov" placeholder="Num. Factura Proveedor" />
			<input name="importe" type="text" class="form-control input_form" id="num_fact_prov" placeholder="Importe €" required="required" />
			<select name="departamento" class="form-control input_form" required="required">
				<option value="0">DEPARTAMENTO</option>
				<?php 
				foreach($departamentos as $departamento) {
				?>
				    <option value="<?php echo $departamento->id . '__' . $departamento->name ?>"><?php echo $departamento->name ?></option>
				<?php 
				}
				?>
            </select>
		</div>
		<button id="crear_factura" type="submit" class="btn btn-primary">Enviar</button>
	</form>
	<!-- <br><br>
	<a href="facturas.php" role="button" class="btn btn-basic">Ver tabla</button> -->
</div>