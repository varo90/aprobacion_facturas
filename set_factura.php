<?php 
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('db_connections.php');
    include('queries.php');
    include('upload.php');
    include('mail.php');
    
    $param = '?success=yes';
    $prov = explode('__',$_POST['num_prov']);
    $num_prov = $prov[0]; $nom_prov = $prov[1];

    $db_my = new db('my','facturas_workflow');

    if(!isset($_POST['num_fact_prov']) || $_POST['num_fact_prov'] == '' || $_POST['num_fact_prov'] == NULL) {
        $id = $db_my->make_query(queries::get_highest_id())[0]->id;
        $num_factura = $id+1;
    } else {
        $num_factura = $_POST['num_fact_prov'];
    }

    $nom_fich = $num_prov.'_'.$num_factura.'.pdf';
    $file = upload::upload_file($_FILES['nueva_factura'],$num_prov.'_'.$num_factura,'pdf','facturas/');
    if(!$file) {
        $param = '?success=no';
        header("location:crear_factura.php$param");
    }
    
    $query = $db_my->conn->prepare(queries::set_factura());
    $depto = explode('__',$_POST['departamento']);
    $num_depto = $depto[0]; $nom_depto = $depto[1];
    try {
        $query->execute([$num_factura,$nom_fich,$num_prov,$nom_prov,$_POST['importe'],$num_depto,$nom_depto]);
    } catch (PDOException $e) {
        $errorinfo = $e->errorInfo[1];
        $param = "?success=$errorinfo";
    } finally {
        unset($db_my);
        $body = 'Nueva factura para departamento de ' . utf8_decode($nom_depto) . '.';
        $attachment = 'facturas/' . $num_prov . '_' . $num_factura . '.pdf';
        $mail = new mail(['asantos@santaeulalia.com'],[],[],'Factura Prov. ' . $num_prov . '. # ' . $num_factura,$body,$attachment);
        header("location:crear_factura.php$param");
    }