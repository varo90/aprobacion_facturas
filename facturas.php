<?php
    include('header.php');
    include('session_init.php');
    $admin = 0;
    if(isset($_SESSION['usergroup_link']) && $_SESSION['usergroup_link'] == 1) {
        $admin = 1;
    }
?>
    <div class="contenedor">
    	<div id="loading"></div>
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                </tr>
            </thead>
            <tfoot>
                <tr>
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>