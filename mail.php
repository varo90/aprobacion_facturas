<?php
include('/var/www/UTILS/PHPMailer/src/SMTP.php');
include('/var/www/UTILS/PHPMailer/src/PHPMailer.php');
    
class mail {
    var $receptor = array();
    var $cc = array();
    var $bcc = array();
    var $subject = '';
    var $body = '';
    var $attachment = '';
    
    function __construct($receptor,$cc,$bcc,$subject,$body,$attachment) {
        $this->receptor=$receptor;
        $this->cc=$cc;
        $this->bcc=$bcc;    //Copia oculta
        $this->subject=$subject;
        $this->body=$body;
        $this->attachment=$attachment;
        $this->send_email();
    }
    
    function send_email() {        
        $mail = new PHPMailer(true);
        //Luego tenemos que iniciar la validación por SMTP:
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = 'smtp.office365.com'; // Aquí pondremos el SMTP a utilizar. Por ej. mail.midominio.com
        $mail->Username = 'it@santaeulalia.com'; // Email de la cuenta de correo. ej.info@midominio.com La cuenta de correo debe ser creada previamente.
        $mail->Password = 'Informatica2013'; // Aqui pondremos la contraseña de la cuenta de correo
        $mail->Port = 587; // Puerto de conexión al servidor de envio.
        $mail->From = 'it@santaeulalia.com'; // Desde donde enviamos (Para mostrar). Puede ser el mismo que el email creado previamente.
        $mail->FromName = 'Solicitudes IT Santa Eulalia'; //Nombre a mostrar del remitente.
        foreach($this->receptor as $mail_address) {
            $mail->AddAddress($mail_address); // Esta es la dirección a donde enviamos
        }
        foreach($this->cc as $mail_address) {
            $mail->AddCC($mail_address);
        }
        foreach($this->bcc as $mail_address) {
            $mail->AddBCC($mail_address);
        }
        $mail->IsHTML(true); // El correo se envía como HTML
        $mail->Subject = utf8_decode($this->subject); // Este es el titulo del email.
        $mail->Body = $this->body; // Mensaje a enviar.
        $mail->AddAttachment($this->attachment);
        try {
            $mail->Send(); // Envía el correo.
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
}