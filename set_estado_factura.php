<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include('session_init.php');
include('db_connections.php');
include('queries.php');

/*****************/

$db_my = new db('my','facturas_workflow');

$dep_dire = $_SESSION['userdpto_link'] == 14 ? 'direcc' : 'dpto';

$query = $db_my->conn->prepare(queries::set_estado_factura($dep_dire));
$query->execute([$_POST['estado'],$_POST['id']]);

unset($db_my);

/*****************/

echo $dep_dire;