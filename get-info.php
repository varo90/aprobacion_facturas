<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('session_init.php');
    include('db_connections.php');
    include('queries.php');
    
    /*****************/
    
    $db_my = new db('my','facturas_workflow');
    $db_ms = new db();
    
    if($_SESSION['userdpto_link'] == 14) {
        $cond = 'WHERE aprobada_dpto=1 ORDER BY aprobada_direcc, fecha_asignado DESC';
    } else {
        if($_SESSION['userdpto_link'] == 10) {
            $cond = '';
        } else if($_SESSION['userid_link'] == 2) {
            $cond = "WHERE proveedor LIKE 'P%' ORDER BY aprobada_dpto, fecha_asignado DESC";
        } else {
            $cond = 'WHERE departamento='.$_SESSION['userdpto_link'].' ORDER BY aprobada_dpto, fecha_asignado DESC';
        }
    }
    $facturas = $db_my->make_query(queries::get_facturas_my($cond),[],PDO::FETCH_ASSOC);
    $get_fact_num = $db_ms->conn->prepare(queries::get_sap_fac_num());
    $insert_fact_num = $db_my->conn->prepare(queries::set_num_factura());
    //echo $_SESSION['userdpto_link'];
    $data = array();
    foreach($facturas as $factura) {
        $prov = explode('(',$factura['Proveedor']);
        $code_prov = end($prov);
        // $name_file = substr($code_prov, 0, -1).'_'.$factura['#Factura Proveedor'].'.pdf';
        // $name_file = $factura['nombre_fichero'];
        $name_file = $factura['CodeProv'] . '_' . $factura['#Factura Proveedor'] . '.pdf';
        $route_file = 'facturas/'.$name_file;
        $id = $factura['id'];
        if(strlen($factura['#Factura SAP']) == 0) {
            $get_fact_num->execute([$factura['CodeProv'],$factura['#Factura Proveedor']]);
            $fact_sap = $get_fact_num->fetchAll(PDO::FETCH_OBJ);
            if(sizeof($fact_sap) > 0) {
                $factura['#Factura SAP'] = $fact_sap[0]->DocNum;
                $get_fact_num->execute([$fact_sap[0]->DocNum,$id]);
            } else {
                $factura['#Factura SAP'] = '---';
            }
        }
        $factura['#Factura SAP'] = strlen($factura['#Factura SAP']) == 0 ? '---' : $factura['#Factura SAP'];
        $btn_aprobar = ($factura['Estado'] == "<font color='green'>Aprobada</font>") || ($factura['Estado'] == "<font color='red'>Denegada por dirección</font>") ? 1 : 0;
        $factura['#Factura Proveedor'] = "<a class=\"watch_bill\" onclick=\"watch_bill('$route_file',$id,$btn_aprobar)\" target=\"_blank\"><span class=\"glyphicon glyphicon-eye-open\"></span> ".$factura['#Factura Proveedor']." (VER)</a>";
        $factura['Importe'] = number_format($factura['Importe'], 2, ',', '.');
        $fechaapr = strtotime($factura['Fecha de asignación']);
        $factura['Aprobada por departamento'] = set_fecha($factura['Aprobada por departamento']);
        $factura['Aprobada por dirección'] = set_fecha($factura['Aprobada por dirección']);
        $factura['Fecha de asignación'] = '<div style="display:none">'.$fechaapr.'</div>'.date('d/m/Y',$fechaapr);
        $data[] = $factura;
    }
    
    unset($db_my);
    unset($db_ms);
    
    /*****************/
    
    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );
    
    echo json_encode($results);
    
    /*****************/
    /*****************/
    
    function set_fecha($fecha) {
        $fecha_int = strtotime($fecha);
        if($fecha_int == -62169984000) {
            return '---';
        } else {
            return '<div style="display:none">'.date('d/m/Y H:i:s',$fecha_int).'</div>'.date('d/m/Y H:i:s',$fecha_int);
        }
    }