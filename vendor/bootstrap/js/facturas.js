$( document ).ready(function() {
	display_table();
	$('#factura_form').onsubmit = function(event) {
        event.preventDefault();
        $('#factura_form').innerHTML = '<img src="images/busy.gif"> Subiendo...';
	}
});

function display_table() {
	if($('#data-info').length == false) { return false; }
	var data = {"query" : 'get_facturas_my'};
	var columns = [];
	var rows = [];
	if ( $.fn.DataTable.isDataTable('#data-info') ) {
	  is_dt = true;
	} else {
		var is_dt = false;
	}
	$.ajax({
        data:  data,
        url:   'get-info.php',
        type:  'post',
        dataType:"json",
        beforeSend: function( xhr ) {
            $('#loading').html('<img src="images/busy.gif" />');
        },
        success:  function (response) {
        	$('#loading img').remove();
        	var jsonPretty = JSON.stringify(response.aaData, null, '\t');
        	var jsonObj = $.parseJSON(jsonPretty);
        	$.each(jsonObj[0], function(key,value){
        		columns.push({ mData: key });
        		if(!is_dt) {
            		$('#data-info thead tr').append( $('<th />', {text : key}) );
            		$('#data-info tfoot tr').append( $('<th />', {text : key}) );
        		}
        	});
        	$.each(jsonObj, function(key,value){
        		rows.push(value);
        	});
        	datatable_info(rows,columns);
        }
    });	
}

function datatable_info(response,columns) {
	$('#data-info').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        //"sAjaxSource": "get-info.php",
        "aaSorting": [[1,'desc']],
        "lengthMenu": [[10, 25, 50, 100, 150, 200, 500, 1000, 2000], [10, 25, 50, 100, 150, 200, 500, 1000, 2000]],
        "oLanguage": {
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        "aoColumns": columns,
        "aaData": response
    });
}