<?php

class queries {
    static function get_facturas() {
        $sql = "SELECT DocNum AS '#Factura SAP', NumAtCard AS '#Factura Proveedor', CardName AS Proveedor, DocTotal AS Importe, DocDate AS Fecha
                FROM OPCH
                WHERE CardCode LIKE 'A%' AND U_GSP_UPDATEDATE>='2018-06-01'";
        return $sql;
    }

    static function get_sap_fac_num() {
        $sql = "SELECT DocNum
                FROM SBO_EULALIA.dbo.OPCH
                WHERE CardCode = ? AND NumAtCard = ? ";
        return $sql;
    }
    
    static function get_facturas_my($cond) {
        $sql = "SELECT id,
                    num_factura_sap AS '#Factura SAP',
                    num_factura_proveedor AS '#Factura Proveedor',
                    nombre_proveedor AS 'Proveedor',
                    proveedor AS CodeProv,
                    importe AS Importe,
                    nombre_departamento AS Departamento,
                    CASE
                        WHEN aprobada_dpto = 0 AND aprobada_direcc = 0 THEN 'Por aprobar departamento'
                        WHEN aprobada_dpto = 1 AND aprobada_direcc = 0 THEN '<font color=''orange''>Por aprobar dirección</font>'
                        WHEN aprobada_dpto = 1 AND aprobada_direcc = 1 THEN '<font color=''green''>Aprobada</font>'
                        WHEN aprobada_dpto = 1 AND aprobada_direcc = 2 THEN '<font color=''red''>Denegada por dirección</font>'
                        WHEN aprobada_dpto = 2 THEN '<font color=''red''>Denegada por departamento</font>'
                    ELSE '---' END AS Estado,
                    fecha_asignado AS 'Fecha de asignación',
                    fecha_aprobacion_dpto AS 'Aprobada por departamento',
                    fecha_aprobacion_direcc AS 'Aprobada por dirección'
                FROM facturas
                $cond";
        return $sql;
    }
    
    static function get_departamentos() {
        $sql = "SELECT * FROM department";
        return $sql;
    }
    
    static function set_factura() {
        $sql = "INSERT INTO facturas(num_factura_proveedor,nombre_fichero,proveedor,nombre_proveedor,importe,departamento,nombre_departamento,fecha_asignado) VALUES (?,?,?,?,?,?,CURRENT_TIMESTAMP())";
        return $sql;
    }

    static function get_highest_id() {
        $sql = "SELECT id FROM facturas ORDER BY id DESC LIMIT 1";
        return $sql;
    }

    static function set_estado_factura($dep_dire) {
        $sql = "UPDATE facturas SET aprobada_$dep_dire=?, fecha_aprobacion_$dep_dire=CURRENT_TIMESTAMP() WHERE id=?";
        return $sql;
    }
    
    static function get_proveedores($cond) {
        $sql = "SELECT CardCode AS id, CardName AS name FROM ocrd WHERE CardCode LIKE '$cond' AND CardName IS NOT NULL";
        return $sql;
    }

    static function set_num_factura() {
        $sql = "INSERT INTO facturas(num_factura_sap) VALUES (?) WHERE id=?";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
