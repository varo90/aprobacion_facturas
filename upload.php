<?php

class upload {
    static function upload_file($file,$name,$extens='',$folder='') {
        $nom_fich = basename($file['name']);
        $ext = explode('.', $nom_fich);
        $extension = end($ext);
        $destination = $folder.$name.'.'.$extension;
        
        if($extens != '' && $extension == $extens) {
            if (move_uploaded_file($file['tmp_name'], $destination)) {
                return $destination;
            } else {
                echo "Fichero no se pudo cargar.";
            }
        } else {
            echo "Fichero no cargado.<br>Debe ser un fichero .$extens.<br>";
            echo 'Extensión: ' . $extension . '<br>';
            echo 'Nombre: ' . $nom_fich;
        }
        return false;
    }
}