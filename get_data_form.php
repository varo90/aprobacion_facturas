<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('db_connections.php');
    include('queries.php');
    
    $db_my = new db('my','employees');
    $departamentos = $db_my->make_query(queries::get_departamentos(),[],PDO::FETCH_OBJ);
    unset($db_my);


    if($_SESSION['userdpto_link'] == 10) {
        $cond = 'A%';
    } else if($_SESSION['userid_link'] == 2) {
        $cond = 'P%';
    }
    $db_ms = new db();
    $proveedores = $db_ms->make_query(queries::get_proveedores($cond),[],PDO::FETCH_OBJ);
    unset($db_ms);